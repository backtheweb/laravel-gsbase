<?php

namespace Backtheweb\GsBase;


use Illuminate\Support\ServiceProvider;

class GsBaseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        if ($this->app->runningInConsole()) {
            $this->publishes([__DIR__ . '/config/gsBase.php' => config_path('gsBase.php')], 'config');
        }

        \Illuminate\Support\Facades\Auth::provider('gsbase', function($app, array $config) {
            return new GsBaseAuthProvider($this->app['hash'], $config['model']);
        });

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/config/gsBase.php', 'gsBase');

        $this->app->bindIf('gsbase.socket', function () {

            $config = $this->app['config']->get('gsBase.socket');

            return new Socket(
                $config['host'],
                $config['port'],
                $config['company'],
                $config['application'],
                $config['exercise'],
                $config['dateType']
            );
        });

        $this->app->bindIf('gsbase.auth', function () {

            $config = $this->app['config']->get('gsBase.auth');

            return new Auth( $config['user'], $config['password'] );
        });

        $this->app->bindIf('gsbase.request', function () {

            return new Request(
                $this->app['gsbase.socket'],
                $this->app['gsbase.auth'],
                $this->app['config']->get('gsBase.window')
            );
        });
    }
}
