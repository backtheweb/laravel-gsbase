<?php

namespace Backtheweb\GsBase\Facades;

use Illuminate\Support\Facades\Facade;

class GsBase extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'gsbase.request';
    }
}
