<?php

namespace Backtheweb\GsBase\Exceptions;

class GsBaseException extends \RuntimeException
{

    protected $message     = 'Server error response';
    protected $code        = '1000';
}
