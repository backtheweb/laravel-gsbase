<?php

namespace Backtheweb\GsBase\Exceptions;

use Exception;

class GsBaseExceptionServer extends Exception {


    protected $message     = 'Server error response';
    protected $code        = '1000';

}
