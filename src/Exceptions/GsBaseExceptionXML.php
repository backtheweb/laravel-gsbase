<?php

namespace Backtheweb\GsBase\Exceptions;

use Exception;

class GsBaseExceptionXML extends Exception {


    protected $message     = 'Invalid XML';
    protected $code        = '1001';
}
