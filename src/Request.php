<?php

namespace Backtheweb\GsBase;

use Illuminate\Support\Facades\Log;

class Request
{
    public function __construct( protected Socket $socket, protected Auth $auth, protected $window = 'a_funcweb'){}

    public function __call(string $name, array|string $arguments) :mixed
    {
        return $this->get($name, $arguments);
    }

    public function get( string $action, array|string $data = null, string $delimiter = ',', string $window = null )
    {
        $params = is_array($data) ? join($delimiter, $data) : $data;

        Log::debug($action);
        Log::debug($params);

        if(null === $window){
            $window = $this->window;
        }

        [$status, $content] = $this->socket->execute(
            $action,
            $window,
            $params,
            $this->auth->user,
            $this->auth->password
        );

        Log::debug($content);

        if($status === -1){
            return (object) [ 'Error' => $content ];
        }

        try {
            $xml = simplexml_load_string($content, "SimpleXMLElement", LIBXML_NOCDATA);
            $xml->preserveWhiteSpace = false;
            $xml->formatOutput       = true;

        } catch (\Exception $e) {
            return (object) [ 'Error' => $e->getMessage()];
        }

        $json = json_encode($xml);

        return json_decode($json,false);
    }
}
