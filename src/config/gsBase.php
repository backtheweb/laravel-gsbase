<?php

return [

    'auth' => [
        'user'          => env('GSBASE_AUTH_USER'),
        'password'      => env('GSBASE_AUTH_PASSWORD'),
    ],

    'socket' => [
        'host'          => env('GSBASE_SOCKET_HOST'),
        'port'          => env('GSBASE_SOCKET_PORT', 8121),
        'company'       => env('GSBASE_SOCKET_COMPANY'),
        'application'   => env('GSBASE_SOCKET_APPLICATION'),
        'exercise'      => env('GSBASE_SOCKET_EXERCISE'),
        'dateType'      => env('GSBASE_SOCKET_DATATYPE', 'xml'),
    ],

    'window'            => env('GSBASE_FUNC_WINDOW',  'a_funcweb'),
    'login_func'        => env('GSBASE_FUNC_LOGIN',   'a_valida_cuenta'),
];

