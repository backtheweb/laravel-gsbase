<?php

/**
 * Description of gsbSocket
 * Funciones para conexión con Servidor gsBase
 * @version 1.5
 */

class gsbSocket
{
    const RESPONSE_SUCCESS         = 0;
    const RESPONSE_ERROR           = -1;
    const CONNEXION_STATUS_SENDING = 3;
    const CONNEXION_STATUS_OPEN    = 1;
    const CONNEXION_STATUS_CLOSED  = 0;
    const DATA_TYPE_JSON           = 'json';
    const DATA_TYPE_PHP            = 'php';
    const DATA_TYPE_XML            = 'xml';

    protected mixed $stream;
    protected string $error = "";
    protected int $status;
    protected string $char;
    protected string $host;
    protected int $port;        // 8121
    protected string $company;
    protected string $app;
    protected string $exercise;
    protected string $logonType = "p_logon";
    protected string $dataType  = self::DATA_TYPE_JSON;

    public function __construct( )
    {
        $this->status = self::CONNEXION_STATUS_CLOSED;
        $this->char   = chr(2);
    }

    /**
     * @param string $host
     * @param int $port
     * @param string $company
     * @param string $app
     * @param string $exercise
     */
    public function config( string $host, int $port, string $company, string $app, string $exercise)
    {
        $this->host     = $host;
        $this->port     = $port;
        $this->company  = $company;
        $this->app      = $app;
        $this->exercise = $exercise;
    }

    public function setTipoLogon($logonType) : void
    {
        $this->logonType = $logonType;
    }

    /**
     * @param string $dataType
     * @throws Exception
     */
    public function setDataType(string $dataType) :void
    {
        $types = [
            self::DATA_TYPE_JSON,
            self::DATA_TYPE_PHP,
            self::DATA_TYPE_XML,
        ];

        if(!in_array($dataType, $types)) {

            throw new \Exception(sprintf(
                'The %s type is not supported in GsBase. Supported types: %s.',
                $dataType,
                join(', ', $types)
            ));
        }

        $this->dataType = $dataType;
    }

    /**
     * @return string
     */
    public function getDataType()
    {
        return $this->dataType;
    }

    /**
     * @param string $action
     * @param string $window
     * @param mixed $args
     * @param $userId
     * @param $userPassword
     * @return array
     */
    public function execute(string $action, string $window, mixed $args, $userId, $userPassword)
    {
        $response = $this->connect($this->host, $this->port);

        if ($response != self::RESPONSE_SUCCESS) {
            return [ self::RESPONSE_ERROR, "NO SE PUDO CONECTAR:" . $this->getLastErrorStr() ];
        }

        //Logon("jjverges","conex","conex","erpnet","eja","","")

        $response = $this->logon($this->company, $userId, $userPassword, $this->app, $this->exercise, "", "");

        if ($response != self::RESPONSE_SUCCESS) {
            $this->disconnect();
            return [ self::RESPONSE_ERROR, "NO SE PUDO HACER LOGON:" . $this->getLastErrorStr() ];
        }

        $args     = serialize($args);
        $response = $this->rgsb($action, $window, $args);
        $this->disconnect();

        if ($response == "") {

            return [self::RESPONSE_ERROR, $this->getLastErrorStr() ?? 'Unknown error'];
        }

        $output = match ($this->dataType) {
            self::DATA_TYPE_JSON    => json_decode($response, true),
            self::DATA_TYPE_PHP     => unserialize($response),
            default                 => $response,
        };

        return [self::RESPONSE_SUCCESS, $output];
    }

    public function getLastErrorStr()
    {
        return $this->error;
    }

    /**
     * @param string $hostname
     * @param int $port
     *
     * @return int
     */
    public function connect(string $hostname, int $port)
    {
        $this->error = "";

        if ($this->status != self::CONNEXION_STATUS_CLOSED) {
            $this->disconnect();
            $this->error = "Error: La conexión no estaba cerrada!";
            return self::RESPONSE_ERROR;
        }

        $errorCode    = "";
        $errorMessage = "";
        $resource     = fsockopen($hostname, $port, $errorCode, $errorMessage);

        if (!$resource) {
            $this->error = "No se pudo conectar. Error: " . $errorCode . " " . $errorMessage;
            return self::RESPONSE_ERROR;
        }

        $response = "";

        while (true) {
            $chunk     = fgets($resource, 2);
            $response .= $chunk;
            if (chr(1) . chr(2) == substr($response, -2)) break;
        }

        $this->stream = $resource;
        $this->status = self::CONNEXION_STATUS_OPEN;

        return self::RESPONSE_SUCCESS;
    }

    public function disconnect()
    {
        $this->error = "";

        try {
            fclose($this->stream);
        } catch (Exception $e) {
            $this->error = "Error al Desconectar: " . $e->getMessage();
            return self::RESPONSE_ERROR;
        }

        $this->status = self::CONNEXION_STATUS_CLOSED;

        return self::RESPONSE_SUCCESS;
    }

    /**
     * @param string $company Empresa Gestora
     * @param string $userId Id del usuario que realiza la consulta
     * @param string $userPassword Contraseña del usuario
     * @param string $app Aplicación sobre la que se va a realizar la consulta
     * @param string $exercise Ejercicio sobre el que se va a realizar la consulta
     * @param string $appPassword Contraseña para acceder a la aplicación
     * @param string $exercisePassword Contraseña para acceder al ejercicio
     * @return int
     */
    public function logon(string $company, string $userId, string $userPassword, string $app, string $exercise, string $appPassword = "", string $exercisePassword = "")
    {
        $response = "";

        $this->send(
            $this->logonType,
            join(',', [
                $company,
                $userId,
                $userPassword,
                $app ,
                $exercise,
                $appPassword,
                $exercisePassword
            ])
            , $response
        );

        if ($this->logonType == "x_logon") {
            $response = unserialize($response);
        }

        $data = explode($this->char, $response);

        if ($data[0] != "Ok") {
            $this->error = $data[0];
            return self::RESPONSE_ERROR;
        }
        return self::RESPONSE_SUCCESS;
    }

    /**
     * @param string $action Accion
     * @param string $window Ventana
     * @param mixed $args Argum
     * @return string
     */
    public function rgsb(string $action, string $window, mixed $args)
    {
        $response = "";
        $funcName = $action . "|" . $window;

        if ($this->send($funcName, $args, $response) != self::RESPONSE_SUCCESS) {
            return "";
        }

        $data = explode($this->char, $response);

        if ($data[1] != "") {
            $this->error = $data[1];
            return "";
        }
        return $data[0];
    }

    /**
     * @param string $funcName
     * @param string $encoded
     * @param mixed $output
     * @return int
     */
    public function send(string $funcName, string $encoded, mixed &$output)
    {
        $encoded        = $funcName . $this->char . $encoded;
        $this->error    = "";
        $this->status   = self::CONNEXION_STATUS_SENDING;
        $hex            = dechex( strlen($encoded) );

        while (strlen($hex) < 6) {
            $hex = "0" . $hex;
        }

        $encoded = $hex . $encoded;

        fputs($this->stream, $encoded);
        $this->get($output);

        $key       = substr($output, -12);
        $signature = ',gsB+' . chr(1) . chr(12) . chr(3) . chr(26) . '...';

        if ($key == $signature) {
            while ($key == $signature) {
                $output = substr($output, 0, -12);
                $this->get($output);
                $key = substr($output, -12);
            }
        }

        return self::RESPONSE_SUCCESS;
    }

    public function get(&$h1b4151238bca)
    {
        $e0a6b5057266e = fgets($this->stream, 7);
        $tf5a8e923f8cd = hexdec($e0a6b5057266e);
        $j2ffe4e77325d = 1024;
        $p568d8e07bbe5 = 0;

        while (true) {
            if ($tf5a8e923f8cd < 1) break;
            if ($tf5a8e923f8cd < $j2ffe4e77325d) $j2ffe4e77325d = $tf5a8e923f8cd;
            $b8d777f385d3d = fgets($this->stream, $j2ffe4e77325d + 1);
            $h1b4151238bca .= $b8d777f385d3d;
            $tf5a8e923f8cd = $tf5a8e923f8cd - strlen($b8d777f385d3d);
            $p568d8e07bbe5 += 1;
        }
    }
}
