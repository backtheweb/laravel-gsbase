<?php

namespace Backtheweb\GsBase;

use SimpleXMLElement;

class Response
{

    public string $status;
    public string $raw;

    public static function make(string $response){

        return new self();
    }
}
