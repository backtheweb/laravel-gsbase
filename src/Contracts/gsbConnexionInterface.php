<?php

interface gsbConnexionInterface
{
    /**
     * @param $action
     * @param $func
     * @param $data
     * @param string $delimiter
     * @return SimpleXMLElement
     */
    public function execute($action, $func, $data, $delimiter  = ',') : SimpleXMLElement;
}

