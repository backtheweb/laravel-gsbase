# Laravel GsBase

## Install

``` bash
$ composer require backtheweb/laravel-gsbase
$ php artisan vendor:publish --provider="Backtheweb\GsBase\GsBaseServiceProvider" --tag="config"
```

Update `config/auth.php

```
    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'gsbase',
        ],
    ],
    'providers' => [
        'gsbase' => [
            'driver' => 'gsbase',
            'model' => App\Models\User::class,
        ],
    ],
```

## Example
``` php
/** @var \SimpleXMLElement $response */
$response = GsBase::get('a_valida_cuenta', 'PACO', 'PACO',);
dump($response);

$response = GsBase::a_valida_cuenta('PACO', 'PACO');
dump($response);
```

